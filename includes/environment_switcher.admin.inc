<?php

/**
 * @file
 * Settings for environment.
 */

/**
 * Menu callback: Settings.
 *
 * Configuration form to change sender mail address and environment.
 */
function environment_settings_form($form, &$form_state) {
  $environments = array(
    'SELECT' => 'Select Environment',
    'DEVELOPMENT' => 'Development',
    'PRODUCTION' => 'Production',
  );
  $environment = variable_get('development_environment_identifier', NULL);
  $form['development_environment_identifier'] = array(
    '#type' => 'select',
    '#title' => t('Select environment'),
    '#default_value' => $environment,
    '#options' => $environments,
    '#required' => TRUE,
  );
  $form['dev_email_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('DEVELOPEMENT EMAIL SETTINGS'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $change_email = variable_get('development_change_email', 0);
  $form['dev_email_settings']['change_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check this to update email address for all emails.'),
    '#default_value' => $change_email,
  );

  $form['dev_email_settings']['dev_email'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $email_address = variable_get('development_environment_email', 'example@gmail.com');
  $form['dev_email_settings']['dev_email']['development_environment_email'] = array(
    '#type' => 'textfield',
    '#title' => t('To email address'),
    '#default_value' => $email_address,
    '#description' => t('If above checkbox is checked, all emails will be forwarded to this email address.'),
    '#required' => TRUE,
  );
  $cc_email_address = variable_get('development_cc_email');
  $form['dev_email_settings']['dev_email']['development_environment_cc_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Cc email address'),
    '#default_value' => $cc_email_address,
    '#description' => t('Add mails ids for cc seperated with commas.'),
    '#required' => TRUE,
  );
  $bcc_email_address = variable_get('development_bcc_email');
  $form['dev_email_settings']['dev_email']['development_environment_bcc_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Bcc email address'),
    '#default_value' => $bcc_email_address,
    '#description' => t('Add mails ids for bcc seperated with commas.'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 * Implements hook_validate().
 */
function environment_settings_form_validate($form, &$form_state) {
  $email = $form_state['values']['development_environment_email'];
  $cc_emails = $form_state['values']['development_environment_cc_email'];
  $bcc_emails = $form_state['values']['development_environment_bcc_email'];
  if (!valid_email_address($email)) {
    form_set_error('development_environment_email', 'Not a valid To email address.');
  }
  $cc_email = explode(',', $cc_emails);
  foreach ($cc_email as $email) {
    if (!valid_email_address($email)) {
      form_set_error('development_environment_cc_email', 'Not a valid cc email address.');
    }
  }
  $bcc_email = explode(',', $bcc_emails);
  foreach ($bcc_email as $email) {
    if (!valid_email_address($email)) {
      form_set_error('development_environment_bcc_email', 'Not a valid bcc email address.');
    }
  }
}

/**
 * Callback for environment settings form.
 */
function environment_settings_form_submit($form, &$form_state) {
  $change_email = $form_state['values']['change_email'];
  $email = $form_state['values']['development_environment_email'];
  $environment = $form_state['values']['development_environment_identifier'];
  $cc_email = $form_state['values']['development_environment_cc_email'];
  $bcc_email = $form_state['values']['development_environment_bcc_email'];
  variable_set('development_environment_email', $email);
  variable_set('development_environment_identifier', $environment);
  variable_set('development_change_email', $change_email);
  variable_set('development_cc_email', $cc_email);
  variable_set('development_bcc_email', $bcc_email);
  if ($environment == 'DEVELOPMENT') {
    variable_set('googleanalytics_account', NULL);
    if (module_exists('domain')) {
      $result = db_query('SELECT dc.* FROM {domain_conf} dc');
      $data = $result->fetchAll();
      foreach ($data as $key => $val) {
        $node_data = unserialize($val->settings);
        if (isset($node_data['googleanalytics_account'])) {
          $node_data['googleanalytics_account'] = "";
          db_update('domain_conf')
            ->fields(array('settings' => serialize($node_data)))
            ->condition('domain_id', $val->domain_id, '=')
            ->execute();
        }
      }
    }
    $dev_modules_selected = variable_get('dev_modules_selected', NULL);
    module_disable($dev_modules_selected);

    $dev_all_module = variable_get('dev_all_module', NULL);
    module_enable($dev_all_module);

  }
  elseif ($environment == 'PRODUCTION') {
    $pro_modules_selected = variable_get('pro_modules_selected', NULL);
    module_disable($pro_modules_selected);

    $pro_all_module = variable_get('pro_all_module', NULL);
    module_enable($pro_all_module);
  }
  drupal_set_message('The configuration are saved.');
}

/**
 * Menu callback: Production Settings.
 *
 * Configuration form to set modules enabled or disabled for production.
 */
function production_environment_settings_form($form, &$form_state) {
  $form['#tree'] = TRUE;
  $form['modules'] = array(
    '#type' => 'fieldset',
    '#title' => t('Modules'),
  );

  // Get all disabled modules.
  $pro_disabled_modules = variable_get('pro_modules_selected', NULL);

  // Get all enabled modules.
  $modules = module_list();
  $pro_module_names = array();
  foreach ($modules as $module) {
    $module_name = str_replace('_', ' ', $module);
    $pro_module_names[$module] = $module_name;
  }
  // Get modules already set for disable.
  if ($pro_disabled_modules) {
    $pro_module_names = array_merge($pro_disabled_modules, $pro_module_names);
  }
  $form['modules']['pro_enabled_module'] = array(
    '#type' => 'select',
    '#options' => $pro_module_names,
    '#default_value' => $pro_disabled_modules,
    '#multiple' => TRUE,
    '#chosen' => TRUE,
    '#title' => 'Select modules to disable',
    '#description' => 'Display only enabled modules.',
  );

  // Get current list of modules.
  $files = system_rebuild_module_data();

  // Remove hidden modules from display list.
  $visible_files = $files;
  foreach ($visible_files as $filename => $file) {
    if (!empty($file->info['hidden'])) {
      unset($visible_files[$filename]);
    }
  }
  module_load_include('inc', 'system', 'system.admin');
  uasort($visible_files, 'system_sort_modules_by_info_name');
  $name = array();
  foreach ($visible_files as $mod) {
    $name[$mod->name] = $mod->info['name'];
  }

  // Get all module to enable.
  $pro_all_module_to_enable = variable_get('pro_all_module', NULL);
  $form['modules']['pro_all_module'] = array(
    '#type' => 'select',
    '#options' => $name,
    '#default_value' => $pro_all_module_to_enable,
    '#multiple' => TRUE,
    '#chosen' => TRUE,
    '#title' => 'Select modules to enable.',
    '#description' => 'Display all modules.',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 * Callback for production settings form.
 */
function production_environment_settings_form_submit($form, &$form_state) {
  $environment = variable_get('development_environment_identifier', NULL);
  // Get selected modules disabled.
  $mod_disabled = variable_get('pro_modules_selected', NULL);
  $pro_modules_selected = $form_state['values']['modules']['pro_enabled_module'];
  $pro_mod_to_enable = array();
  foreach ($mod_disabled as $modules) {
    if (!in_array($modules, $pro_modules_selected)) {
      $pro_mod_to_enable[$modules] = $modules;
    }
  }
  $pro_all_module = $form_state['values']['modules']['pro_all_module'];
  $common_modules = array_intersect($pro_all_module, $pro_modules_selected);
  if ($common_modules) {
    drupal_set_message('Cant enable and disable a module at same time', 'error');
  }
  else {
    variable_set('pro_modules_selected', $pro_modules_selected);

    variable_set('pro_all_module', $pro_all_module);

    if ($environment == 'PRODUCTION') {

      module_enable($pro_mod_to_enable);

      module_disable($pro_modules_selected);

      module_enable($pro_all_module);

      drupal_set_message('The configuration are saved.');
    }

    elseif ($environment == 'DEVELOPMENT') {
      drupal_set_message('No permission for update data', 'error');
    }
  }

}

/**
 * Menu callback: Development Settings.
 *
 * Configuration form to set modules enabled or disabled for development.
 */
function development_environment_settings_form($form, &$form_state) {
  $form['#tree'] = TRUE;
  $form['modules'] = array(
    '#type' => 'fieldset',
    '#title' => t('Modules'),
  );

  // Get all disabled modules.
  $dev_disabled_modules = variable_get('dev_modules_selected', NULL);

  // Get all enabled modules.
  $modules = module_list();
  $dev_module_names = array();
  foreach ($modules as $module) {
    $module_name = str_replace('_', ' ', $module);
    $dev_module_names[$module] = $module_name;
  }
  // Get modules already set for disable.
  if ($dev_disabled_modules) {
    $dev_module_names = array_merge($dev_disabled_modules, $dev_module_names);
  }
  $form['modules']['dev_enabled_module'] = array(
    '#type' => 'select',
    '#options' => $dev_module_names,
    '#default_value' => $dev_disabled_modules,
    '#multiple' => TRUE,
    '#chosen' => TRUE,
    '#title' => 'Select modules to disable',
    '#description' => 'Display only enabled modules.',
  );

  // Get current list of modules.
  $files = system_rebuild_module_data();

  // Remove hidden modules from display list.
  $visible_files = $files;
  foreach ($visible_files as $filename => $file) {
    if (!empty($file->info['hidden'])) {
      unset($visible_files[$filename]);
    }
  }
  module_load_include('inc', 'system', 'system.admin');
  uasort($visible_files, 'system_sort_modules_by_info_name');
  $name = array();
  foreach ($visible_files as $mod) {
    $name[$mod->name] = $mod->info['name'];
  }
  // Get all module to enable.
  $dev_all_module_to_enable = variable_get('dev_all_module', NULL);

  $form['modules']['dev_all_module'] = array(
    '#type' => 'select',
    '#options' => $name,
    '#default_value' => $dev_all_module_to_enable,
    '#multiple' => TRUE,
    '#chosen' => TRUE,
    '#title' => 'Select modules to enable.',
    '#description' => 'Display all modules.',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 * Callback for development settings form.
 */
function development_environment_settings_form_submit($form, &$form_state) {
  $environment = variable_get('development_environment_identifier', NULL);
  // Get selected modules disabled.
  $mod_disabled = variable_get('dev_modules_selected', NULL);
  $dev_modules_selected = $form_state['values']['modules']['dev_enabled_module'];
  $dev_mod_to_enable = array();
  foreach ($mod_disabled as $modules) {
    if (!in_array($modules, $dev_modules_selected)) {
      $dev_mod_to_enable[$modules] = $modules;
    }
  }

  $dev_all_module = $form_state['values']['modules']['dev_all_module'];

  $common_modules = array_intersect($dev_all_module, $dev_modules_selected);
  if ($common_modules) {
    drupal_set_message('Cant enable and disable a module at same time', 'error');
  }
  else {
    variable_set('dev_modules_selected', $dev_modules_selected);

    variable_set('dev_all_module', $dev_all_module);

    if ($environment == 'DEVELOPMENT') {

      module_enable($dev_mod_to_enable);

      module_disable($dev_modules_selected);

      module_enable($dev_all_module);

      drupal_set_message('The configuration are saved.');
    }
    elseif ($environment == 'PRODUCTION') {
      drupal_set_message('No permission for update data', 'error');
    }
  }
}
