Environment Switcher

Overview
--------
Environment Switcher module work as switcher  between development and production environment. We can pre define modules which are enabled or disabled in environments. Also can identify in which environment we are working.

Installation steps.

1. Download Environmnet switcher from https://www.drupal.org/project/environment_switcher.
2. Download Chosen module.
3. Download Chosen library from https://harvesthq.github.io/chosen/ andf pace it in sites/all/libraries.
4. Set configurations for chosen.
5. Configure Development settings in configuration under Development section.
